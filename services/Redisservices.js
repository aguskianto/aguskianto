const redis = require('redis');
const redisconfig = require("../config/redisconfig.js");

const uri = redisconfig.uri;
var redisClient = redis.createClient(uri);

redisClient.on('error', (err) => {
    console.log("Error " + err);
});

const Mongoservices = require("./Mongoservices.js");
const appconfig = require('../config/appconfig.js');

exports.clear = async function clear(key) {
    let val = [];
   
    redisClient.del(key);
   
    redisClient.get(key, async function (err, res) {
        if (err) {
            val = [];
        } else if (res) {
            val = JSON.parse(res);
        }
    });

    // race condition for async
    let promiseRedis = new Promise(resolve => {
        let waitRedis = setTimeout(() => {
            clearTimeout(waitRedis);

            resolve(val);
        }, appconfig.connecttimeout);
    });

    return Promise.race([promiseRedis]);
};

exports.getUsers = async function getUsers() {
    let results = [];

    redisClient.get('users', async function (err, res) {
        if (err) {
            results = [];
        } else if (res) {
            const rawResults = JSON.parse(res);

            if (rawResults.length === 0) {
                results = await Mongoservices.fetchUsers();

                redisClient.set('users', JSON.stringify(results));
                redisClient.expire('users', 7200);
            } else {
                results = rawResults;
            }
        } else {
            results = await Mongoservices.fetchUsers();

            redisClient.set('users', JSON.stringify(results));
            redisClient.expire('users', 7200);
        }
    });

    // race condition for async
    let promiseRedis = new Promise(resolve => {
        let waitRedis = setTimeout(() => {
            clearTimeout(waitRedis);

            if (results.length > 0) {
                resolve(results);
            }
        }, appconfig.connecttimeout);
    });

    let promiseMongo = new Promise(resolve => {
        let waitMongo = setTimeout(() => {
            clearTimeout(waitMongo);

            resolve(results);
        }, appconfig.dbtimeout);
    });

    return Promise.race([promiseRedis, promiseMongo]);
};

exports.getUserByAccountNumber = async function getUserByAccountNumber(accountNumber) {
    let results = [];

    redisClient.get('accountNumber:' + accountNumber, async function (err, res) {
        if (err) {
            results = [];
        } else if (res) {
            const rawResults = JSON.parse(res);

            if (rawResults.length === 0) {
                results = await Mongoservices.fetchUserByAccountNumber(accountNumber);

                redisClient.set('accountNumber:' + accountNumber, JSON.stringify(results));
                redisClient.expire('accountNumber:' + accountNumber, 7200);
            } else {
                results = rawResults;
            }
        } else {
            results = await Mongoservices.fetchUserByAccountNumber(accountNumber);

            redisClient.set('accountNumber:' + accountNumber, JSON.stringify(results));
            redisClient.expire('accountNumber:' + accountNumber, 7200);
        }
    });

    // race condition for async
    let promiseRedis = new Promise(resolve => {
        let waitRedis = setTimeout(() => {
            clearTimeout(waitRedis);

            if (results.length > 0) {
                resolve(results);
            }
        }, appconfig.connecttimeout);
    });

    let promiseMongo = new Promise(resolve => {
        let waitMongo = setTimeout(() => {
            clearTimeout(waitMongo);

            resolve(results);
        }, appconfig.dbtimeout);
    });

    return Promise.race([promiseRedis, promiseMongo]);
};

exports.getUserByidentityNumber = async function getUserByidentityNumber(identityNumber) {
    let results = [];

    redisClient.get('identityNumber:' + identityNumber, async function (err, res) {
        if (err) {
            results = [];
        } else if (res) {
            const rawResults = JSON.parse(res);

            if (rawResults.length === 0) {
                results = await Mongoservices.fetchUserByidentityNumber(identityNumber);

                redisClient.set('identityNumber:' + identityNumber, JSON.stringify(results));
                redisClient.expire('identityNumber:' + identityNumber, 7200);
            } else {
                results = rawResults;
            }
        } else {
            results = await Mongoservices.fetchUserByidentityNumber(identityNumber);

            redisClient.set('identityNumber:' + identityNumber, JSON.stringify(results));
            redisClient.expire('identityNumber:' + identityNumber, 7200);
        }
    });

    // race condition for async
    let promiseRedis = new Promise(resolve => {
        let waitRedis = setTimeout(() => {
            clearTimeout(waitRedis);

            if (results.length > 0) {
                resolve(results);
            }
        }, appconfig.connecttimeout);
    });

    let promiseMongo = new Promise(resolve => {
        let waitMongo = setTimeout(() => {
            clearTimeout(waitMongo);

            resolve(results);
        }, appconfig.dbtimeout);
    });

    return Promise.race([promiseRedis, promiseMongo]);
};

exports.destroy = async function destroy() {
    redisClient.quit();
};

exports.flush = async function flush() {
    redisClient.flushall();
};