const Tokenservices = require("../services/Tokenservices.js");

appconfig = {
    port: process.env.PORT || 3000,
    timeout: parseInt(process.env.TIMEOUT) || 4000,
    connecttimeout: parseInt(process.env.CONNECTTIMEOUT) || 300,
    dbtimeout: parseInt(process.env.DBTIMEOUT) || 1500,
    token: Tokenservices.generate()
}

module.exports = appconfig;