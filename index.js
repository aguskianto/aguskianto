const express = require("express");
const chalk = require('chalk');
const morgan = require('morgan');
const cors = require("cors");
const bodyParser = require("body-parser");

var app = express();

app.use(morgan('tiny'));
app.use(bodyParser.json({limit: "8mb"}));

app.use(cors({
    credentials: true
}));

/* app configuration */
const appconfig = require('./config/appconfig.js');
const port = appconfig.port;
const timeout = appconfig.timeout;
const connecttimeout = appconfig.connecttimeout;
const dbtimeout = appconfig.dbtimeout;

/* Mongo Services */
const Mongoservices = require("./services/Mongoservices.js");

/* Redis Services */
const Redisservices = require("./services/Redisservices.js");

function checkToken (req, res, next) {
    const appToken = appconfig.token;
    const rawToken = req.get('Authorization');

    let token = '';

    if (rawToken !== undefined && rawToken !== null) {
        let token = rawToken.split(' ')[1];

        if (rawToken !== undefined && rawToken !== null && appToken === token) {
            next()
        } else {
            res.json({message: "You are not authorized"});
        }
    } else {
        res.json({message: "You are not authorized"});
    }
}

app.get("/", async function(req, res) {
    res.json({message: "Hello from test btpn apps"});
});

app.get("/users", checkToken, async function(req, res) {
    Mongoservices.connect()
    .catch(error => {
        console.log(error);
    });

    setTimeout(async function () {
        if (!Mongoservices.isConnected()) {
          return res.json({feedback: "Something wrong with DB connection"});
        }
    
        // get user data from redis
        const users = await Redisservices.getUsers()
        .catch(error => {
            return res.json({ feedback: "Something wrong with query process" });
        });

        return res.json(users);
    }, dbtimeout);
});

app.get("/users/accountNumber/:accountNumber", checkToken, async function(req, res) {
    const accountNumber = req.params.accountNumber;

    Mongoservices.connect()
    .catch(error => {
        console.log(error);
    });

    setTimeout(async function () {
        if (!Mongoservices.isConnected()) {
          return res.json({feedback: "Something wrong with DB connection"});
        }
    
        // get user data
        const user = await Redisservices.getUserByAccountNumber(accountNumber)
        .catch(error => {
          return res.json({ feedback: "Something wrong with query process" });
        });
    
        return res.json(user);
    }, dbtimeout);
});

app.get("/users/identityNumber/:identityNumber", checkToken, async function(req, res) {
    const identityNumber = req.params.identityNumber;

    Mongoservices.connect()
    .catch(error => {
        console.log(error);
    });

    setTimeout(async function () {
        if (!Mongoservices.isConnected()) {
          return res.json({feedback: "Something wrong with DB connection"});
        }
    
        // get user data
        const user = await Redisservices.getUserByidentityNumber(identityNumber)
        .catch(error => {
          return res.json({ feedback: "Something wrong with query process" });
        });
    
        return res.json(user);
    }, dbtimeout);
});

app.post("/users", checkToken, async function(req, res) {
    const param = req.body;

    Mongoservices.connect()
    .catch(error => {
        console.log(error);
    });

    setTimeout(async function () {
        if (!Mongoservices.isConnected()) {
          return res.json({feedback: "Something wrong with DB connection"});
        }
    
        // get user data
        const feedback = await Mongoservices.postUser(param)
        .catch(error => {
          return res.json({ feedback: "Something wrong with query process" });
        });

        Redisservices.flush();
    
        return res.json(feedback);
    }, dbtimeout);
});

app.post("/users/:id", checkToken, async function(req, res) {
    const id = req.params.id;
    const param = req.body;

    Mongoservices.connect()
    .catch(error => {
        console.log(error);
    });

    setTimeout(async function () {
        if (!Mongoservices.isConnected()) {
          return res.json({feedback: "Something wrong with DB connection"});
        }
    
        // get user data
        const feedback = await Mongoservices.putUser(id, param)
        .catch(error => {
          return res.json({ feedback: "Something wrong with query process" });
        });

        Redisservices.flush();
    
        return res.json(feedback);
    }, dbtimeout);
});

app.delete("/users/:id", checkToken, async function(req, res) {
    const id = req.params.id;
   
    Mongoservices.connect()
    .catch(error => {
        console.log(error);
    });

    setTimeout(async function () {
        if (!Mongoservices.isConnected()) {
          return res.json({feedback: "Something wrong with DB connection"});
        }
    
        // get user data
        const feedback = await Mongoservices.deleteUser(id)
        .catch(error => {
          return res.json({ feedback: "Something wrong with query process" });
        });

        Redisservices.flush();
    
        return res.json(feedback);
    }, dbtimeout);
});

app.get("/token", function(req, res) {
    const token = appconfig.token;

    return res.json(token);
});

const server = app.listen(port, () => {
    console.log(`listening on port ${chalk.red(port)}`);
});

server.timeout = timeout;

/* shutdown gracefully */
process.on('SIGTERM', () => {
    debug('SIGTERM signal received.');
    debug('Closing http server.');
    server.close(() => {
      debug('Http server closed.');
      Redisservices.destroy();
      Mongoservices.destroy()
      .then(() => {
        process.exit(0);
      });
    });
});