mongoconfig = {
    uri: process.env.MONGODB_URI,
    database: process.env.DATABASE || 'heroku_qmrslp9g'
}

module.exports = mongoconfig;