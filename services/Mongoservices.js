const MongoClient = require('mongodb').MongoClient;
const mongoconfig = require("../config/mongoconfig.js");

const URL = mongoconfig.uri;

let mongoclient;

var options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
};

exports.isConnected = function isConnected() {
    let connectionStatus = false;

    if (!!mongoclient) {
        if (!!mongoclient.topology) {
            if (mongoclient.topology.isConnected()) {
                connectionStatus = true;
            }
        }
    }

    return connectionStatus;
}

exports.connect = async function connect () {
    try {
        if (!this.isConnected()) {
            mongoclient = await MongoClient.connect(URL, options);
        }

        return mongoclient;
    } catch (error) {
        throw (error);
    }
}

exports.fetchUsers = async function fetchUsers() {
    try {
        const db = mongoclient.db(`${encodeURIComponent(mongoconfig.database)}`);
        const collection = db.collection('user_data');

        const user$ = collection.find().sort({'_id': 1}).project({
            Id: 1,
            userName: 1,
            accountNumber: 1,
            emailAddress: 1,
            identityNumber: 1
        }).toArray();

        const users = await user$;
       
        return users;
    } catch (error) {
        throw error;
    }
};

exports.fetchUserByAccountNumber = async function fetchUserByAccountNumber(accountNumber) {
    try {
        const db = mongoclient.db(`${encodeURIComponent(mongoconfig.database)}`);
        const collection = db.collection('user_data');

        let query = {'$and': [{'accountNumber': accountNumber}]};

        const user$ = collection.find(query).sort({'_id': 1}).project({
            Id: 1,
            userName: 1,
            accountNumber: 1,
            emailAddress: 1,
            identityNumber: 1
        }).toArray();

        const user = await user$;
       
        return user;
    } catch (error) {
        throw error;
    }
};

exports.fetchUserByidentityNumber = async function fetchUserByidentityNumber(identityNumber) {
    try {
        const db = mongoclient.db(`${encodeURIComponent(mongoconfig.database)}`);
        const collection = db.collection('user_data');

        let query = {'$and': [{'identityNumber': identityNumber}]};

        const user$ = collection.find(query).sort({'_id': 1}).project({
            Id: 1,
            userName: 1,
            accountNumber: 1,
            emailAddress: 1,
            identityNumber: 1
        }).toArray();

        const user = await user$;
       
        return user;
    } catch (error) {
        throw error;
    }
};

exports.postUser = async function postUser(param) {
    try {
        const db = mongoclient.db(`${encodeURIComponent(mongoconfig.database)}`);
        const collection = db.collection('user_data');

        collection.insertOne(param, function(err, res) {
            if (err) throw err;
        });
       
        return {message: "User added"};
    } catch (error) {
        throw error;
    }
};

exports.putUser = async function putUser(id, param) {
    try {
        const db = mongoclient.db(`${encodeURIComponent(mongoconfig.database)}`);
        const collection = db.collection('user_data');

        let query = {'$and': [{'_id': id}]};

        collection.updateOne(query, param, function(err, res) {
            if (err) throw err;
        });
       
        return {message: "User updated"};
    } catch (error) {
        throw error;
    }
};

exports.deleteUser = async function deleteUser(id) {
    try {
        const db = mongoclient.db(`${encodeURIComponent(mongoconfig.database)}`);
        const collection = db.collection('user_data');

        let query = {'$and': [{'_id': id}]};

        collection.deleteOne(query, function(err, res) {
            if (err) throw err;
        });
       
        return {message: "User deleted"};
    } catch (error) {
        throw error;
    }
};